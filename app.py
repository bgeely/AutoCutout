from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from datetime import timedelta

app = Flask(__name__)
from koutu import *

app.register_blueprint(index_koutu)

from yanzhi import *

app.register_blueprint(index_yanzhi)

from meiyan import *

app.register_blueprint(index_meiyan)

from zhengjianzhao import *

app.register_blueprint(index_zhengjianzhao)


Bootstrap(app)

# 设置静态文件缓存过期时间
app.send_file_max_age_default = timedelta(seconds=1)


# 首页
@app.route('/', methods=['POST', 'GET'])  # 添加路由
def index():
    return render_template('koutu.html')


@app.route('/error', methods=['POST', 'GET'])  # 添加路由
def error():
    return render_template('404.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
