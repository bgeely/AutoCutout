# 1创建一个蓝图对象
from flask import Blueprint, render_template

index_example = Blueprint("example", __name__)


# 2注册路由
# @app.route('/edit')改为
@index_example.route('/example')
def example():
    return render_template('example.html')
